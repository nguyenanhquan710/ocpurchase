package com.automation.core;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Class to create an app instance
 */
public class WebAppManager {

	private static ConcurrentHashMap<Long, WebApp> instances = new ConcurrentHashMap<Long, WebApp>();

	// Get WebApp instance
	public static WebApp getWebApp(Class<?> clazz) throws Exception {
		// get thread id
		Long id = Thread.currentThread().getId();
		// check WebApp instance created for above thread
		if (!instances.containsKey(id)) {
			instances.put(id, (WebApp) clazz.newInstance());
		}
		// get WebApp instance if already exists
		if (instances.containsKey(id)) {
			return instances.get(id);
		}
		// create new WebApp instance since none exists
		return (WebApp) clazz.newInstance();
	}

	// Quit WebApp
	public static synchronized void quitApp() {
		// get thread id
		Long id = Thread.currentThread().getId();
		WebApp app = instances.get(id);
		if (app != null) {
			instances.remove(id);
			app.quitApp();
		}
	}

	public static synchronized void takeAppScreenshots(String testName) throws Exception {
		// get thread id
		Long id = Thread.currentThread().getId();
		WebApp app = instances.get(id);
		if (app != null) {
			app.takeScreenshot(testName);
		}
	}

}