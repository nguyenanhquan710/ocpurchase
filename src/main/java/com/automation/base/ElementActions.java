package com.automation.base;

/**
 * Enumerated type for element actions to be done using wait mechanism implemented in WebApp
 * Enum elements must correspond with processing in methods they are used in (e.g., WebApp.safeAction)
 */
public enum ElementActions {
    NO_ACTION,
    CLICK,
    CLEAR,
    SEND_KEYS,
    SELECT_OPTION,
    HOVER,
    SCROLL
}