package com.automation.utils;

import java.security.SecureRandom;

public class StringUtils {
	/**
	 * Parses a price from a string
	 */
	public static double parsePrice(String price) throws Exception {
		price = price.replace(",", "");
		price = price.replace("TTC", "");
		price = price.replace("€", "");
		price = price.replaceAll("\\s", "");
		int decpoint = price.indexOf(".");
		if (decpoint >=0)
			price = price.substring(0, decpoint + 2);
		return Double.parseDouble(price);
	}

	/**
	 * Get the method name in call stack. <br />
	 * Utility function
	 *
	 * @return method name
	 */
	public static String getMethodName() {
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		return ste[ste.length - 1 - 0].getMethodName();
	}

	/**
	 * Get the caller method name in call stack. <br />
	 * Utility function
	 *
	 * @return method name
	 */
	public static String getCallerMethodName() {
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		return ste[ste.length - 1 - 1].getMethodName();
	}

	/**
	 * Generates random string with the given length
	 *
	 * @param len
	 *            length of the string
	 * @return random string
	 */
	public static String randomString(int len) {
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	/**
	 * Generates random numeric string with the given length
	 *
	 * @param len
	 *            length of the string
	 * @return random numeric string
	 */
	public static String randomNumericString(int len) {
		String AB = "0123456789";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	/**
	 * Generates random alphabetical string with the given length
	 *
	 * @param len
	 *            length of the string
	 * @return random alphabetical string
	 */
	public static String randomAlphabeticalString(int len) {
		String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}
}
