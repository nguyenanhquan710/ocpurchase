package com.automation.utils;

import java.text.DecimalFormat;
import java.text.ParseException;

public class DoubleUtils {

	// Constructor
	public static double parsePrice(String price) throws Exception {
		price = price.replace(',', '.');
		price = price.replace("TTC", "");
		price = price.replace("€", "");
		price = price.replaceAll("\\s", "");
		price = price.replace("CHF", "");
		price = price.replace("'", "");
		int decpoint = price.indexOf(".");
		price = price.substring(0, decpoint + 3);
		return Double.parseDouble(price);
	}

	/**
	 * Rounds double value precision to compare value
	 */
	public static double roundDoubleValue(double value) throws ParseException {
		DecimalFormat df = new DecimalFormat("0.00");
		String formate = df.format(value);
		return df.parse(formate).doubleValue();
	}
}