Feature: Lotte.vn

  @purchase @lotte
  Scenario: As a user I want to track a general link for Lotte
    Given I open full product link to Lotte
    And I add click checkout button on Lotte Product page
    And I proceed to checkout page of Lotte
    And I log in at cart page on Lotte
    And I select address at checkout page on Lotte
    And I choose payment method and cancel payment on Lotte