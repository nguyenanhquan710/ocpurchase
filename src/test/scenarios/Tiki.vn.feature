Feature: Tiki.vn

  Scenario: As a user I want to track a general link for Tiki
    Given I open full product link to Tiki
    And I add product to cart on Tiki
    When I proceed to checkout page on Tiki
    And I log in at checkout page on Tiki
    And I select address at checkout page on Tiki
    And I cancel payment on Tiki