package com.pageobjects.tiki;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class TikiCartPage extends WebPageBase {
	private static final By CART_PAGE = By.cssSelector(".tiki-cart");
	private static final By CHECKOUT_BUTTON = By.cssSelector(".btn-checkout");

	public TikiCartPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(CART_PAGE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickToCheckout() throws Exception {
		try {
			app.safeClick(CHECKOUT_BUTTON);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
}
