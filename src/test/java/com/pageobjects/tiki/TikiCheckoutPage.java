package com.pageobjects.tiki;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;
import com.testdata.TestData;
import com.unity.Domain;

public class TikiCheckoutPage extends WebPageBase {
	private static final By CHECKOUT_PAGE = By.cssSelector(".tiki-shipping, .tiki-payment");

	private static final By OPTION_LOGIN = By.cssSelector(".nav-register a[alt=login-form]");
	private static final By INPUT_EMAIL = By.cssSelector("#popup-login-email");
	private static final By INPUT_PASSWORD = By.cssSelector("#login_password");
	private static final By LOGIN_BUTTON = By.cssSelector("#login_popup_submit");

	private static final By FULLNAME_INPUT = By.cssSelector("#full_name");
	private static final By PHONE_INPUT = By.cssSelector("#telephone");
	private static final By CITY_INPUT = By.cssSelector("#region_id");
	private static final By DISTRICT_INPUT = By.cssSelector("#city_id");
	private static final By WARD_INPUT = By.cssSelector("#ward_id");
	private static final By ADDRESS_INPUT = By.cssSelector("#street");
	private static final By ADDRESS_TYPE_INDIVIDUAL = By.cssSelector("input.icheck[value='1']");
	private static final By ADDRESS_TYPE_ORGANISATION = By.cssSelector("input.icheck[value='2']");
	private static final By SUBMIT_ADDRESS_BUTTON = By.cssSelector("#btn-address");
	private static final By SELECT_CURRENT_ADDRESS = By.cssSelector(".saving-address");

	private static final By TOTAL_AMOUNT = By.cssSelector(".total2 span");
	
	private static final By PLACE_ORDER_BUTTON = By.cssSelector("#btn-placeorder");

	private static final By ORDER_ID = By.cssSelector(".well-sm");

	public TikiCheckoutPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(CHECKOUT_PAGE);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	// STEP 1: LOGIN
	public void clickLogInTab() throws Exception {
		try {
			app.safeClick(OPTION_LOGIN);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void inputEmailPassword(String email, String password) throws Exception {
		try {
			app.safeInput(INPUT_EMAIL, email);
			app.safeInput(INPUT_PASSWORD, password);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickLogInButton() throws Exception {
		try {
			app.safeClick(LOGIN_BUTTON);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	// STEP 2: ADDRESS
	public void inputAddress() throws Exception {
		try {
			app.safeInput(FULLNAME_INPUT, TestData.readUser(Domain.TIKI, "FULLNAME"));
			app.safeInput(PHONE_INPUT, TestData.readUser(Domain.TIKI, "PHONE"));
			app.safeSelect(CITY_INPUT, TestData.readUser(Domain.TIKI, "CITY"));
			app.safeSelect(DISTRICT_INPUT, TestData.readUser(Domain.TIKI, "DISTRICT"));
			app.safeSelect(WARD_INPUT, TestData.readUser(Domain.TIKI, "WARD"));
			app.safeInput(ADDRESS_INPUT, TestData.readUser(Domain.TIKI, "ADDRESS"));
			switch (TestData.readUser(Domain.TIKI, "ADDRESS_TYPE")) {
				case "1":
					app.safeClick(ADDRESS_TYPE_INDIVIDUAL);
					break;
				case "2":
					app.safeClick(ADDRESS_TYPE_ORGANISATION);
					break;
				default:
			}
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickSubmitAddress() throws Exception {
		try {
			app.safeClick(SUBMIT_ADDRESS_BUTTON);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public boolean isAddressSaved() throws Exception {
		try {
			return app.isElementDisplayed(SELECT_CURRENT_ADDRESS);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickSelectThisAddress() throws Exception {
		try {
			app.safeClick(SELECT_CURRENT_ADDRESS);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	// STEP 3: ORDER
	public void clickPlaceOrder() throws Exception {
		try {
			app.safeClick(PLACE_ORDER_BUTTON);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	
	public double getTotalAmount() throws Exception {
		try {
			String text = app.safeGetText(TOTAL_AMOUNT);
			text = text.replace(".", "");
			text = text.replace(" ₫", "");
			double value = Double.parseDouble(text);
			return value;
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	// STEP SUCCESS
	public String getOrderId() throws Exception {
		try {
			return app.safeGetText(ORDER_ID);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
}
