package com.pageobjects.tiki;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class TikiCategoryPage extends WebPageBase {
	private static final By CATEGORYPAGE = By.cssSelector(".tiki-categories");
	private static final By ACTIVE_RECOMENDED_ITEM = By.cssSelector(".product-listing .product-box:nth-of-type(2) .swiper-slide-active a");

	public TikiCategoryPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(CATEGORYPAGE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickRecommendedItem() throws Exception {
		try {
			app.safeClick(ACTIVE_RECOMENDED_ITEM);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}

	}

}
