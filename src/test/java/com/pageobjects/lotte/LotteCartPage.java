package com.pageobjects.lotte;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class LotteCartPage extends WebPageBase {
	//private static final By CART_PAGE = By.cssSelector(".checkout");
	private static final By CHECKOUT_BUTTON = By.cssSelector(".checkout-methods-items button");
	private static final By EMAIL_INPUT = By.cssSelector(".login-modal-popup._show  #email");
	private static final By PASSWORD_INPUT = By.cssSelector(".login-modal-popup._show  #pass");
	private static final By LOGIN_BUTTON = By.cssSelector("#send2");
   // private static final By LOGIN_POPUP=By.cssSelector(".login-modal-popup._show .modal-inner-wrap");
    
	public LotteCartPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(CHECKOUT_BUTTON);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	//click button checkout
	public void clickToCheckout() throws Exception {
		try {
			app.waitForAllCalls();
			app.safeClick(CHECKOUT_BUTTON);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	
	public void inputEmailPassword(String email, String password) throws Exception {
		try {
			app.safeInput(EMAIL_INPUT, email);
			app.safeInput(PASSWORD_INPUT, password);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickLogInButton() throws Exception {
		try {
			app.safeClick(LOGIN_BUTTON);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	

	
}
