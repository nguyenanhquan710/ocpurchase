package com.pageobjects.lotte;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class LotteProductPage extends WebPageBase {

	private static final By PRODUCT_PAGE = By.cssSelector(".catalog-product-view");
	private static final By LOADING_IMAGE = By.cssSelector(".loading");
	private static final By CHECKOUT_BUTTON = By.cssSelector("button.btn-checkout");
	private static final By POPUP_OVERLAY_CLOSE = By.cssSelector("#popup-banner-bottom .banner-l .btn-close");

	public LotteProductPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(PRODUCT_PAGE);
			app.safeElementWaitToDisappear(LOADING_IMAGE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	public boolean isPopupDisplayed() throws Exception {
		try {
			return app.isElementDisplayed(POPUP_OVERLAY_CLOSE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
	public void closeAnyPopup() throws Exception {
		try {
			if (app.isElementDisplayed(POPUP_OVERLAY_CLOSE)) {
				app.safeClick(POPUP_OVERLAY_CLOSE);
			}
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickCheckOutButton() throws Exception {
		try {
			app.safeClick(CHECKOUT_BUTTON);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
}
