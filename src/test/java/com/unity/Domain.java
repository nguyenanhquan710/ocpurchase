package com.unity;

public enum Domain {
	ADAYROI("adayroi.com"),
	FPTSHOP("fptshop.com.vn"),
	LAZADA("lazada.vn"),
	LOTTE("lotte.vn"),
	NAMA("nama.vn"),
	NGUYENKIM("nguyenkim.com"),
	TIKI("tiki.vn");
	
	private String domainLink;

	Domain(String a) {
		this.setDomain(a);
	}


	public String getDomainLink() {
		return domainLink;
	}

	public void setDomain(String domain) {
		this.domainLink = domain;
	}

	public Domain getDomain(String link) {
		for (Domain domain : Domain.values()) {
			if (domain.getDomainLink().equals(link))
				return domain;
		}
		return null;
	}
}
