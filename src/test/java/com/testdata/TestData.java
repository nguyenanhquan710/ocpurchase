package com.testdata;

import java.io.InputStream;
import java.util.HashMap;

import org.yaml.snakeyaml.Yaml;

import com.automation.utils.DataLoader;
import com.unity.Domain;

public class TestData extends DataLoader {
	/**
	 * Reads the content from a given yml file and transforms it into a HashMap
	 * Make sure the yml file content is properly structured
	 * 
	 * @param file
	 *            is the path to yml file to read from
	 * @paran site is the specified site to use the data
	 * @param field
	 *            is the data content to be read
	 */
	protected static String readYAMLValues(String file, Domain site, String field) throws Exception {
		InputStream input = null;
		try {
			Yaml yaml = new Yaml();
			HashMap<String, Object> data = null;
			input = TestData.class.getClassLoader().getResourceAsStream(file);
			data = (HashMap<String, Object>) yaml.load(input);
			HashMap<String, Object> dataPerSite = (HashMap<String, Object>) data.get(site.toString());
			String result = dataPerSite.get(field).toString();
			return result;
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

	public static String readLink(Domain site, String field) throws Exception {
		return readYAMLValues("Link.yml", site, field);
	}

	public static String readUser(Domain site, String field) throws Exception {
		return readYAMLValues("User.yml", site, field);
	}
}
